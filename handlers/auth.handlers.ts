import db from "@/lib/core/db";
import bcryptjs from "bcryptjs";
import { StatusCodes } from "http-status-codes";

import type {
  Credentials,
  EndpointHandlder,
  SignUpCredentials,
} from "@/lib/types";
import {
  PrismaClientInitializationError,
  PrismaClientKnownRequestError,
  PrismaClientValidationError,
} from "@prisma/client/runtime/library";

export const Login: EndpointHandlder = async (Request, Response) => {
  try {
    const credentials: Credentials = Request.body;

    const user = await db.user.findUnique({
      where: {
        username: credentials.username,
      },
    });

    if (user == null) {
      return Response.status(StatusCodes.NOT_FOUND).json({
        reason: "user not found",
      });
    }

    const passowrdIsMatch = await bcryptjs.compare(
      credentials.password,
      user.password
    );

    if (!passowrdIsMatch) {
      return Response.status(StatusCodes.UNAUTHORIZED).json({
        reason: "invalid credentials",
      });
    }

    const { password, ...r } = user;

    return Response.status(StatusCodes.OK).json({
      user: r,
    });
  } catch (error) {
    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, something went wrong",
    });
  }
};

export const SignUp: EndpointHandlder = async (Request, Response) => {
  const signUpCredentials: SignUpCredentials = Request.body;

  try {
    if (
      (await db.user.count({
        where: { username: signUpCredentials.username },
      })) > 0
    ) {
      return Response.status(StatusCodes.CONFLICT).json({
        reason: "username Already exists",
      });
    }

    const user = await db.user.create({
      data: { ...signUpCredentials },
    });

    const { password, ...r } = user;

    return Response.status(StatusCodes.CREATED).json({
      user: r,
    });
  } catch (error) {
    if (
      error instanceof PrismaClientKnownRequestError ||
      error instanceof PrismaClientValidationError
    ) {
      return Response.status(StatusCodes.BAD_REQUEST).json({
        reason: error.message,
        cause: error.cause,
      });
    }

    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, smthg went wrong",
    });
  }
};
