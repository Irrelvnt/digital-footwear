import db from "@/lib/core/db";
import zod from "zod";
import { StatusCodes } from "http-status-codes";
import type { EndpointHandlder, ProductDTO } from "@/lib/types";
import {
  PrismaClientInitializationError,
  PrismaClientKnownRequestError,
  PrismaClientValidationError,
} from "@prisma/client/runtime/library";
import { ValidateObject } from "@/lib/utils";

export const GetAllProducts: EndpointHandlder = async (Request, Response) => {
  try {
    const products = await db.product.findMany();
    return Response.status(StatusCodes.OK).json({
      products,
    });
  } catch (error) {
    console.log(error);
    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, something went wrong",
    });
  }
};

export const GetProduct: EndpointHandlder = async (Request, Response) => {
  try {
    const product = await db.product.findUnique({
      where: {
        id: Request.query.id as string,
      },
    });

    if (product == null) {
      return Response.status(StatusCodes.NOT_FOUND).json({
        reason: "Product does not or no longer exists",
      });
    }

    return Response.status(StatusCodes.OK).json({ product });
  } catch (error) {
    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, something went wrong",
    });
  }
};

export const CreateProduct: EndpointHandlder = async (Request, Response) => {
  try {
    const productData: ProductDTO = Request.body;
    try {
      const userId = Request.user?.id;
      if (userId == null) {
        return Response.status(StatusCodes.UNAUTHORIZED).json({
          reason: "Unauthorized",
        });
      }
      const user = await db.user.findUnique({
        where: {
          id: userId,
        },
      });

      const product = await db.product.create({
        data: { ...productData, seller: { connect: { id: user?.id } } },
      });

      return Response.status(StatusCodes.CREATED).json({
        product,
      });
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError ||
        error instanceof PrismaClientValidationError
      ) {
        return Response.status(StatusCodes.BAD_REQUEST).json({
          reason: error.message,
          cause: error.cause,
        });
      }

      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "oops, smthg went wrong",
      });
    }
  } catch (error) {
    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, something went wrong",
    });
  }
};

export const GetCategories: EndpointHandlder = async (Request, Response) => {
  try {
    const productData: ProductDTO = Request.body;

    try {
      const categories = await db.product.findMany({
        select: {
          category: true,
        },
        distinct: ["category"],
      });

      return Response.status(StatusCodes.CREATED).json({
        categories: categories.map((c) => c.category),
      });
    } catch (error) {
      if (
        error instanceof PrismaClientKnownRequestError ||
        error instanceof PrismaClientValidationError
      ) {
        return Response.status(StatusCodes.BAD_REQUEST).json({
          reason: error.message,
          cause: error.cause,
        });
      }

      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "oops, smthg went wrong",
      });
    }
  } catch (error) {
    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, something went wrong",
    });
  }
};
