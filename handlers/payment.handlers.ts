import db from "@/lib/core/db";
import type { EndpointHandlder, PaymentDTO } from "@/lib/types";
import {
  PrismaClientInitializationError,
  PrismaClientKnownRequestError,
  PrismaClientValidationError,
} from "@prisma/client/runtime/library";
import { StatusCodes } from "http-status-codes";

export const CreatePayment: EndpointHandlder = async (Request, Response) => {
  try {
    const orderDTO: PaymentDTO = Request.body;

    const product = await db.product.findUnique({
      where: {
        id: orderDTO.productId,
      },
    });

    if (product == null) {
      return Response.status(StatusCodes.BAD_REQUEST).json({
        reason: "product not found",
      });
    }

    const order = await db.order.create({
      data: {
        ...orderDTO,
        price: product.price,
      },
    });

    return Response.status(StatusCodes.CREATED).json({
      order,
    });
  } catch (error) {
    if (
      error instanceof PrismaClientKnownRequestError ||
      error instanceof PrismaClientValidationError
    ) {
      return Response.status(StatusCodes.BAD_REQUEST).json({
        reason: error.message,
        cause: error.cause,
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, smthg went wrong",
    });
  }
};

export const GetAllPayments: EndpointHandlder = async (Request, Response) => {
  try {
    const orders = await db.order.findMany();
    return Response.status(StatusCodes.OK).json({
      orders,
    });
  } catch (error) {
    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, something went wrong",
    });
  }
};
