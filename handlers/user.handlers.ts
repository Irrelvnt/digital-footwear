import db from "@/lib/core/db";
import { EndpointHandlder } from "@/lib/types";
import { PrismaClientInitializationError } from "@prisma/client/runtime/library";
import { StatusCodes } from "http-status-codes";

export const GetUser: EndpointHandlder = async (Request, Response) => {
  try {
    const user = await db.user.findUnique({
      where: {
        id: Request.query.id as string,
      },
    });

    if (user == null) {
      return Response.status(StatusCodes.NOT_FOUND).json({
        reason: "User doesn't exist",
      });
    }

    return Response.status(StatusCodes.OK).json({ user });
  } catch (error) {
    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, something went wrong",
    });
  }
};

export const GetAllUsers: EndpointHandlder = async (Request, Response) => {
  try {
    const users = await db.user.findMany();
    return Response.status(StatusCodes.OK).json({
      users,
    });
  } catch (error) {
    if (error instanceof PrismaClientInitializationError) {
      return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
        reason: "db connection failure",
      });
    }

    return Response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      reason: "oops, something went wrong",
    });
  }
};
