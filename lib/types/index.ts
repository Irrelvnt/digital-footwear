import zod from "zod";
import bcryptjs from "bcryptjs";

import type { Product } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";

export type EndpointHandlder<T = any> = (
  Request: NextApiRequest,
  Response: NextApiResponse<T | { [key: string]: any }>
) => Promise<void>;

export class ObjectParseSuccessResult<T> {
  public readonly success: true = true;
  constructor(public readonly payload: T) {}
}

export class ObjectParseFailResult {
  public readonly success: false = false;
  constructor(public readonly reasons: { [key: string]: any }[]) {}
}

// Request types

export const CredentialsObjectValidator = zod.object({
  username: zod.string().min(1),
  password: zod.string().min(1),
});
export type Credentials = zod.infer<typeof CredentialsObjectValidator>;

export const SignUpObjectValidator = zod.object({
  username: zod.string().min(1),
  password: zod
    .string()
    .min(1)
    .transform((arg) => {
      const salt = bcryptjs.genSaltSync(10);
      return bcryptjs.hashSync(arg, salt);
    }),
  email: zod.string().email(),
  isSeller: zod.boolean(),
  adress: zod.string().min(1),
});
export type SignUpCredentials = zod.infer<typeof SignUpObjectValidator>;

export const productDTOValidator = zod.object({
  name: zod.string().min(1),
  image: zod.string(),
  price: zod.object({
    amount: zod.number().nonnegative(),
    currency: zod.string().min(1),
  }),
  description: zod.string(),
  category: zod.string().min(1),
});
export type ProductDTO = zod.infer<typeof productDTOValidator>;

export const PaymentDTOValidator = zod.object({
  userId: zod.string().min(1),
  productId: zod.string().min(1),
});
export type PaymentDTO = zod.infer<typeof PaymentDTOValidator>;

//

//Response types
export type User = {
  id: string;
  email: string;
  username: string;
  isSeller: boolean;
  adress: string;
};
