import type { NextApiRequest } from "next";

declare module "next" {
  export interface NextApiRequest {
    user:
      | {
          id: string;
          email: string;
          username: string;
        }
      | undefined;
  }
}
