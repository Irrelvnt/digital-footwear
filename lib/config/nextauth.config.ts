import db from "@/lib/core/db";
import CredentialsProvider from "next-auth/providers/credentials";

import { PrismaAdapter } from "@next-auth/prisma-adapter";
import type { AuthOptions } from "next-auth";
import axios from "axios";
import { User } from "../types";

export default {
  adapter: PrismaAdapter(db),
  session: {
    strategy: "jwt",
  },
  providers: [
    CredentialsProvider({
      id: "credentials",
      name: "credentials",
      credentials: {
        username: { type: "text" },
        password: { type: "password" },
      },
      async authorize(credentials, req) {
        try {
          const { data } = await axios.post<{ user: User }>(
            "http://0.0.0.0:3000/api/auth/login",
            {
              username: credentials?.username,
              password: credentials?.password,
            }
          );

          if (!data) return null;
          return data.user;
        } catch (error: any) {
          throw new Error(error?.response?.data?.reason);
        }
      },
    }),
  ],
  pages: {
    signIn: "/login",
    error: "/login",
  },
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    async jwt({ token, user }) {
      if (user) {
        token.id = user.id;
        token.username = user.username;
        token.email = user.email;
      }
      return Promise.resolve(token);
    },

    async session({ token, session }) {
      session.user.id = token.id;
      session.user.username = token.username;
      session.user.email = token.email;
      return Promise.resolve(session);
    },
  },
} as AuthOptions;
