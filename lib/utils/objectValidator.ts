import { z } from "zod";
import { ObjectParseFailResult, ObjectParseSuccessResult } from "@/lib/types";

export function ValidateObject<T>(
  object: object,
  validator: z.AnyZodObject
): ObjectParseSuccessResult<T> | ObjectParseFailResult {
  try {
    const parseResult = validator.parse(object) as T;
    return new ObjectParseSuccessResult<T>(parseResult);
  } catch (error: any) {
    const errorLogs = error.issues.map((issue: any) => {
      return { path: issue.path[0], detail: issue.message };
    });
    return new ObjectParseFailResult(errorLogs);
  }
}
