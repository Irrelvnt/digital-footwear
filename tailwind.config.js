/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontFamily: { ox: ["Oxanium", "sans-serif"] },
      backgroundImage: {
        bg: "url('/bg.jpg')",
      },
    },
  },
  plugins: [],
};
