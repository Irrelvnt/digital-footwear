import { FaStar } from "react-icons/fa";
import { submitRating } from "@/utils/submitRating";

export default function Stars({
  rating,
  sellerAdress,
  buyerAdress,
}: {
  rating: number;
  sellerAdress: string | null;
  buyerAdress: string | null;
}): JSX.Element {
  const intger = parseInt(rating.toString());
  const dec = rating - intger;
  const handleRating = (rating: number) => {
    if (!sellerAdress || !buyerAdress) {
      return;
    }
    submitRating(buyerAdress, sellerAdress, rating);
  };

  return (
    <div className="relative flex text-white">
      {Array.from({ length: intger }).map((_, index) => (
        <FaStar size={21} key={index} />
      ))}
      <div className="absolute flex inset-0 opacity-20 z-0">
        {Array.from({ length: 5 }).map((_, index) => (
          <FaStar
            size={21}
            key={index}
            onClick={() => handleRating(index + 1)}
            className="hover:fill-green-500 z-40 cursor-pointer relative"
          />
        ))}
      </div>
      {dec === 0 ? null : <FaStar size={21} />}
    </div>
  );
}
