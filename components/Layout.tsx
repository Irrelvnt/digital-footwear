import { useToggle } from "@/hooks/useToggle";
import SideBar from "./SideBar";
import TopBar from "./TopBar";

export default function Layout({
  children,
}: {
  children: React.ReactNode | JSX.Element;
}) {
  const [isSideBarOpen, toggleSideBar] = useToggle(false);

  return (
    <div className="flex md:flex-none max-w-screen min-h-screen overflow-hidden">
      <div className="fixed z-0 inset-0 bg-bg" />
      <div className="w-screen md:hidden fixed top-0 z-20">
        <div
          style={{
            transform: isSideBarOpen ? "translateX(75vw)" : "translateX(0)",
          }}
          className="transition"
        >
          <TopBar IsSideBarOpen={isSideBarOpen} toggleSideBar={toggleSideBar} />
        </div>
      </div>
      <div className="fixed min-w-[14rem] top-0 w-[75vw] -left-[75vw] h-screen md:left-0 md:w-56 xl:w-72 z-30 transition ">
        <div
          style={{
            transform: isSideBarOpen ? "translateX(75vw)" : "translateX(0)",
          }}
        >
          <SideBar />
        </div>
      </div>
      <div className="relative z-0 w-screen xl:left-72 md:left-56 md:w-[calc(100vw-14rem)] xl:w-[calc(100vw-18rem)]">
        <div
          className="relative z-20 px-4 xl:px-10 w-full pt-24 md:pt-16 lg:pt-20 transition"
          style={{
            transform: isSideBarOpen ? "translateX(75vw)" : "translateX(0)",
          }}
        >
          {children}
        </div>
      </div>
    </div>
  );
}
