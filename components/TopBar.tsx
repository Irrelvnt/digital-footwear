import { Spin as Hamburger } from "hamburger-react";
import { useSession } from "next-auth/react";

export default function TopBar({
  IsSideBarOpen,
  toggleSideBar,
}: {
  IsSideBarOpen: boolean;
  toggleSideBar: () => void;
}) {
  const session = useSession();
  const user = session.data?.user;
  return (
    <div className="py-4 px-8 relative font-ox flex items-center justify-between backdrop-blur-sm bg-white/10 bg-opacity-70">
      <div
        className="w-full h-full absolute top-0 left-0 rounded-t-lg border-b-[2px] border-white"
        style={{
          WebkitMaskImage:
            "linear-gradient(90deg, transparent 0%, rgba(255,255,255,0.2) 100%)",
        }}
      />
      <div className="flex items-center space-x-4">
        <Hamburger
          toggled={IsSideBarOpen}
          toggle={toggleSideBar}
          direction="right"
          color="white"
        />
        <p className="text-white font-semibold font-ox text-lg italic">
          digital-footwear
        </p>
      </div>
      <div className="flex items-center space-x-2 text-white">
        {user?.username}
      </div>
    </div>
  );
}
