import { Product } from "@prisma/client";
import Image from "next/image";
import Link from "next/link";
import { FaShoppingCart } from "react-icons/fa";

const productItem = (product: Product, idx: number) => (
  <Link href={`/shop/${product.id}`} key={idx}>
    <div className="relative h-56 lg:h-64 rounded-2xl cursor-pointer hover:border-white/50 hover:border-2 transition border-transparent border-2 overflow-clip bg-white/20 text-white font-ox font-semibold">
      <div className="relative h-1/2 lg:h-3/5">
        <div className="absolute pr-2 z-30 bottom-2 right-2 w-fit font-semibold text-white text-base flex items-center bg-slate-700/80 rounded-full pl-2">
          {product.price.amount} {product.price.currency}
        </div>
        <Image
          src={product.image}
          className="relative z-20 object-cover overflow-clip bg-transparent bg-white"
          layout="fill"
          alt={product.name}
        />
        <div className="absolute z-0 w-full h-full top-0 left-0 flex items-center justify-center bg-slate-700/60" />
      </div>
      <div className="h-1/2 flex flex-col px-3 py-2 overflow-clip">
        <div className="fill-white text-base flex justify-between">
          <p className="line-clamp-2 text-sm">{product.name}</p>
        </div>
        <div className="flex-grow flex items-center justify-between w-full py-2 font-light pr-3 break-words text-sm -mt-1">
          <div className="bottom-8 w-full absolute top-0 left-0 bg-gradient-to-t from-black/20 to-transparent z-10" />
        </div>
        <div className="absolute z-30 bottom-0 px-6 left-0 flex items-center justify-center space-x-3 bg-gradient-to-tr from-purple-700 to-purple-400 hover:from-purple-600 hover:to-purple-300 w-full py-3">
          <FaShoppingCart className="text-white mb-0.5" />
          <p className="text-white">BUY</p>
        </div>
      </div>
    </div>
  </Link>
);
export default productItem;
