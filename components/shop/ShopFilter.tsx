import { Fragment } from "react";
import { Transition, Menu } from "@headlessui/react";
import { FaFilter } from "react-icons/fa";

const ShopFilter = (values: string[], selected: string, handleChange: any) => {
  return (
    <Menu as="div" className="relative w-full inline-block text-left">
      <Menu.Button className="text-white bg-white/20 transition hover:border-white/50 w-full group px-4 h-[3rem] py-2 flex flex-row focus:outline-none focus:ring focus:ring-none space-x-3 items-center rounded-lg border-2 border-transparent">
        <p className="font-semibold w-full text-xs lg:text-sm text-center">
          {selected}
        </p>
        <FaFilter className="fill-white relative z-20" />
      </Menu.Button>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0"
        enterTo="transform opacity-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100"
        leaveTo="transform opacity-0"
      >
        <Menu.Items className="absolute overflow-y-auto overflow-x-clip max-h-[8rem] left-0 z-30 mt-2 w-full md:w-[120%] origin-top-right rounded-lg bg-white shadow-lg ring-2 ring-white focus:outline-none">
          {values.map((filter: string, idx: number) => (
            <Menu.Item key={idx}>
              {() => (
                <button
                  onClick={handleChange}
                  name="filter"
                  type="button"
                  value={filter}
                  className="bg-white/20 hover:bg-white/30 block px-4 py-2 text-sm w-full first:rounded-tl-lg last:rounded-bl-lg text-purple-600 font-semibold"
                >
                  {filter}
                </button>
              )}
            </Menu.Item>
          ))}
        </Menu.Items>
      </Transition>
    </Menu>
  );
};
export default ShopFilter;
