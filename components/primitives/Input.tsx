import { DetailedHTMLProps, InputHTMLAttributes } from "react";

export default function Input(
  props: DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >
) {
  return (
    <div className="relative rounded-lg w-full h-fit bg-white/10">
      <input
        className="relative w-full z-10 px-4 py-3 bg-transparent text-white focus:ring-0 focus:outline-none placeholder:text-white/70 placeholder:capitalize placeholder:font-ox placeholder:font-medium"
        {...props}
      />
    </div>
  );
}
