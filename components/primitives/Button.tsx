import { ButtonHTMLAttributes, DetailedHTMLProps } from "react";

export default function Button(
  props:
    | DetailedHTMLProps<
        ButtonHTMLAttributes<HTMLButtonElement>,
        HTMLButtonElement
      > & { text: string }
) {
  return (
    <button
      {...props}
      className="relative bg-white/80 group border-box focus:outline-none focus:ring-none text-purple-700 py-[0.6rem] px-[2.5rem] rounded-lg"
    >
      <p className="relative z-10 transition">{props.text}</p>
      <div className="absolute z-0 left-0 top-0 w-full h-full opacity-0 bg-[#00A3FF] rounded-lg transition" />
    </button>
  );
}
