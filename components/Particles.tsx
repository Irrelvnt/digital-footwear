import { Particles as ParticlesJS } from "@blackbox-vision/react-particles";

export default function Particles() {
  return (
    <ParticlesJS
      id="simple"
      width="auto"
      height="100%"
      className="bg-transparent"
      params={{
        particles: {
          number: {
            value: 90,
            density: {
              enable: true,
              value_area: 800,
            },
          },
          shape: {
            type: "circle",
            stroke: {
              width: 0,
              color: "#000000",
            },
            polygon: {
              nb_sides: 5,
            },
          },
          size: {
            value: 3,
          },
          color: {
            value: "#fff",
          },
        },
        interactivity: {
          events: {
            onhover: {
              enable: true,
              mode: "repulse",
            },
          },
        },
      }}
    />
  );
}
