import { signOut, useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { BiSolidLogOutCircle } from "react-icons/bi";
import { FaHome, FaStore, FaUser } from "react-icons/fa";
import classNames from "../utils/classNames";
import { PiSneakerFill } from "react-icons/pi";
import { User } from "@prisma/client";
import { useEffect, useState } from "react";
import axios from "axios";

type sidebarItem = {
  label: string;
  href: string;
  icon: any;
};

const items: sidebarItem[] = [
  { icon: FaHome, label: "Home", href: "/" },
  { icon: FaStore, label: "Shop", href: "/shop" },
];

const bottomItems: sidebarItem[] = [
  { icon: FaUser, label: "PROFILE", href: "/profile" },
];

export default function SideBar() {
  const session = useSession();
  const [user, setUser] = useState<User | null>(null);
  useEffect(() => {
    if (session.status === "authenticated") {
      axios
        .get("/api/users/" + session.data?.user?.id)
        .then((res) => {
          setUser(res.data.user);
          if (router.asPath == "/list-a-product" && !res.data.user.isSeller) {
            router.push("/shop");
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [session]);
  const router = useRouter();
  const item = (item: sidebarItem, idx: number) => (
    <Link href={item.href} key={idx}>
      <div
        className={classNames(
          router.asPath == item.href ||
            (router.asPath.startsWith(item.href) && item.href != "/")
            ? "bg-gradient-to-r from-white/20 to-transparent"
            : "opacity-40 hover:opacity-100 hover:bg-gradient-to-r hover:from-white/10 hover:to-transparent md:hover:cursor-pointer",
          "relative w-full rounded-xl select-none md:transition flex items-center py-3 pl-4 space-x-10 my-4"
        )}
      >
        <item.icon className="fill-white w-6 h-6" />
        <p className="font-semibold text-lg text-white font-ox">{item.label}</p>
      </div>
    </Link>
  );
  return (
    <div className="relative w-full py-4 px-6 bg-white/10 h-screen">
      <div className="w-full h-32 flex flex-col items-center justify-center">
        <Image
          src="/logo.png"
          alt="digital-footwear"
          style={{ WebkitFilter: "invert(100%)" }}
          width={100}
          height={100}
          className="w-20 h-20 object-cover"
        />
        <p className="text-white font-bold font-ox">DIGITAL FOOTWEAR</p>
      </div>
      <div className="w-full bg-gradient-to-r from-transparent via-white to-transparent h-[1px]" />
      <div className="w-full pt-8">
        {items.map(item)}
        {user?.isSeller && (
          <Link href={"/list-a-product"}>
            <div
              className={classNames(
                router.asPath == "/list-a-product"
                  ? "bg-gradient-to-r from-white/20 to-transparent"
                  : "opacity-40 hover:opacity-100 hover:bg-gradient-to-r hover:from-white/10 hover:to-transparent md:hover:cursor-pointer",
                "relative w-full rounded-xl select-none md:transition flex items-center py-3 pl-4 space-x-10 my-4"
              )}
            >
              <PiSneakerFill className="fill-white w-6 h-6" />
              <p className="font-semibold text-lg text-white font-ox">
                Listing
              </p>
            </div>
          </Link>
        )}
      </div>
      <div className="absolute w-[calc(100%-3rem)] space-y-4 bottom-4">
        {bottomItems.map(item)}

        {session.status === "authenticated" && (
          <div
            onClick={() => {
              signOut();
            }}
            className="hover:bg-white/10 md:hover:cursor-pointer w-full rounded-xl select-none md:transition flex items-center py-3 pl-4 space-x-10"
          >
            <BiSolidLogOutCircle className="fill-[#FF7272] w-6 h-6" />
            <p className="font-semibold font-ox text-lg text-[#FF7272]">
              LOGOUT
            </p>
          </div>
        )}
      </div>
    </div>
  );
}
