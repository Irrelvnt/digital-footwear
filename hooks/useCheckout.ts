import { ProductDTO } from "@/lib/types";
import { useState } from "react";

type form = {
  name: string;
  email: string;
  item: ProductDTO | null;
};

const useCheckout = () => {
  const [form, setForm] = useState<form>({
    name: "",
    email: "",
    item: null,
  });
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
  };
  return { form, handleChange, handleSubmit };
};

export default useCheckout;
