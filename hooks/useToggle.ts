import { useState } from "react";

export function useToggle(initial: boolean = false): [boolean, () => void] {
  const [t, setT] = useState<boolean>(initial);

  return [t, () => setT((p) => !p)];
}
