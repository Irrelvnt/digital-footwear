import { User } from "@/lib/types";
import axios, { AxiosError } from "axios";
import { StatusCodes } from "http-status-codes";
import { useRouter } from "next/router";
import { useState } from "react";

type form = {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
};

const useRegister = () => {
  const router = useRouter();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [form, setForm] = useState<form>({
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
  });
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (form.password !== form.confirmPassword) {
      setError("passwords do not match");
      return;
    }
    try {
      const res = await axios.post<User>("/api/auth/sign-up", {
        ...form,
      });

      if (res.status === StatusCodes.OK) {
        router.replace("/login");
      }
    } catch (error) {
      if (error instanceof AxiosError) {
        setError(error.response?.data.reason);
      }
    }
  };
  return { loading, handleChange, handleSubmit, error, setError };
};

export default useRegister;
