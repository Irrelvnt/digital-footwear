// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

contract Reputation {
    struct Rating {
        address rater;
        uint256 rating;
    }

    mapping(address => Rating[]) public reputationScores;

    event RatingSubmitted(
        address indexed rater,
        address indexed ratee,
        uint256 rating
    );

    function submitRating(address ratee, uint256 rating) external {
        require(rating >= 1 && rating <= 5, "Rating must be between 1 and 5");

        Rating memory newRating = Rating(msg.sender, rating);
        reputationScores[ratee].push(newRating);

        emit RatingSubmitted(msg.sender, ratee, rating);
    }

    function getRating(
        address user
    ) external view returns (uint256 totalRating, uint256 count) {
        Rating[] memory ratings = reputationScores[user];

        for (uint256 i = 0; i < ratings.length; i++) {
            totalRating += ratings[i].rating;
        }

        count = ratings.length;

        return (totalRating, count);
    }
}
