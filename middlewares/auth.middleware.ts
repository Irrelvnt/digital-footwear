import { StatusCodes } from "http-status-codes";
import { getServerSession } from "next-auth";
import nextAuthOptions from "@/lib/config/nextauth.config";

import type { NextApiRequest, NextApiResponse } from "next/types";
import type { NextHandler } from "next-connect";

export async function AuthGuard(
  Request: NextApiRequest,
  Response: NextApiResponse,
  next: NextHandler
) {
  const session = await getServerSession(Request, Response, nextAuthOptions);

  if (!session)
    return Response.status(StatusCodes.UNAUTHORIZED).json({
      reason: "Unauthenticated",
    });

  Request.user = session.user;
  await next();
}
