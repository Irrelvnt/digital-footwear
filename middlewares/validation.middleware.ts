import { z } from "zod";
import { StatusCodes } from "http-status-codes";
import { ValidateObject } from "@/lib/utils";

import type { NextApiRequest, NextApiResponse, NextApiHandler } from "next";

export async function ValidationMiddleWare<T>(
  Request: NextApiRequest,
  Response: NextApiResponse,
  _next: NextApiHandler,
  validator: z.AnyZodObject
) {
  const validationResult = ValidateObject<T>(Request.body, validator);
  if (!validationResult.success) {
    return Response.status(StatusCodes.UNPROCESSABLE_ENTITY).json({
      reason: "Request body validation Failed",
      details: validationResult.reasons,
    });
  }
  Request.body = validationResult.payload;
  await _next(Request, Response);
}
