import web3 from "./web3";

const contractAddress = `${process.env.NEXT_PUBLIC_CONTRACT_ADRESS}`;
const contractABI = require("@/reputation/build/contracts/Reputation.json").abi;

export const getRating = async (ratee) => {
  const contract = new web3.eth.Contract(contractABI, contractAddress);

  try {
    const ratings = await contract.methods.getRating(ratee).call();
    if (ratings.count == 0) return null;
    const rating = ratings.totalRating / ratings.count;
    return { value: parseFloat(rating), count: parseInt(ratings.count) };
  } catch (error) {
    console.error("Error fetching rating:", error);
    return null;
  }
};
