import web3 from "./web3";

const contractAdress = process.env.NEXT_PUBLIC_CONTRACT_ADRESS;

const contractABI = require("@/reputation/build/contracts/Reputation.json").abi;

export const submitRating = async (rater, ratee, rating) => {
  const contract = new web3.eth.Contract(contractABI, contractAdress);

  await contract.methods.submitRating(ratee, rating).send({
    from: rater,
    gas: 6000000,
  });
};
