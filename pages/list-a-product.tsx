import Layout from "@/components/Layout";
import Button from "@/components/primitives/Button";
import Input from "@/components/primitives/Input";
import axios from "axios";

import Image from "next/image";
import { useState } from "react";

export default function ListProduct() {
  const [productDetails, setProductDetails] = useState({
    name: "",
    price: {
      amount: 0.0,
      currency: "ETH",
    },
    image: "",
    description: "",
    category: "",
  });
  const handleChange = (e: any) => {
    if (e.target.name === "price") {
      if (isNaN(e.target.value)) return;
      setProductDetails({
        ...productDetails,
        price: {
          amount: parseFloat(e.target.value),
          currency: "ETH",
        },
      });
    } else {
      setProductDetails({ ...productDetails, [e.target.name]: e.target.value });
    }
  };
  const handleSubmit = (e: any) => {
    e.preventDefault();
    try {
      axios
        .post("/api/products/create", productDetails)
        .then((res) => {
          console.log(res);
        })
        .then(() => {
          setProductDetails({
            name: "",
            price: {
              amount: 0,
              currency: "ETH",
            },
            image: "",
            description: "",
            category: "",
          });
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <Layout>
      <div className="lg:px-20 lg:mt-4 font-ox mb-6">
        <div className="relative flex flex-col w-full bg-[#BABDCB26] rounded-[40px]">
          <div
            className="absolute inset-0 z-0 border-[2px] border-white rounded-[40px]"
            style={{
              WebkitMaskImage:
                "linear-gradient(90deg, transparent 0%, rgba(255,255,255,0.2) 100%)",
            }}
          />
          <div className="h-14 lg:h-16 relative rounded-t-[40px] overflow-hidden flex items-center justify-center lg:bg-white/10">
            <div
              className="absolute top-0 left-0 h-full rouned-t-[40px] w-full border-[2px] border-white"
              style={{
                WebkitMaskImage:
                  "linear-gradient(90deg, transparent 0%, rgba(255,255,255,0.2) 100%)",
              }}
            />
            <p className="font-ox text-white font-bold">List new product</p>
          </div>
          {/* flex */}
          <form onSubmit={handleSubmit} className="h-full p-6">
            <div className="relative px-6 py-4 h-fit rounded-xl bg-white bg-opacity-5">
              <div
                className="absolute h-full top-0 left-0 w-full z-0 border-[2px] border-white rounded-lg"
                style={{
                  WebkitMaskImage:
                    "linear-gradient(90deg, rgba(255,255,255,0.3) 0%, rgba(255,255,255,0.1) 100%)",
                }}
              />
              <div className="flex space-x-6 w-full relative">
                <div className="relative w-16 h-16 min-w-[4rem] rounded-xl bg-gradient-to-tr from-white/30 to-white/20 border-2 border-white">
                  <Image
                    src="/logo.png"
                    alt="digital-footwear"
                    layout="fill"
                    className="object-cover"
                    style={{ WebkitFilter: "invert(100%)" }}
                  />
                </div>
                <div className="flex-grow space-y-2">
                  <p className="text-white font-semibold">
                    Product information
                  </p>
                  <p className="text-white text-sm font-semibold">General</p>
                  <div className="flex space-x-3">
                    <Input
                      placeholder="Product name"
                      name="name"
                      onChange={handleChange}
                      minLength={2}
                    />
                    <Input
                      placeholder="price (ETH)"
                      name="price"
                      onChange={handleChange}
                      minLength={1}
                    />
                  </div>
                  <p className="text-white text-sm font-semibold">Image URL</p>
                  <Input
                    placeholder="image"
                    name="image"
                    onChange={handleChange}
                  />
                  <p className="text-white text-sm font-semibold">
                    Description
                  </p>
                  <Input
                    placeholder="Description"
                    onChange={handleChange}
                    name="description"
                  />
                  <p className="text-white text-sm font-semibold">Category</p>
                  <Input
                    placeholder="category"
                    onChange={handleChange}
                    name="category"
                  />
                  <div className="flex justify-end">
                    <Button type="submit" text="List Product" />
                  </div>
                </div>
                {productDetails.image.length > 0 ? (
                  <Image
                    src={productDetails.image}
                    alt="product"
                    width={200}
                    height={200}
                    className="rounded-xl hidden lg:block w-44 h-44 object-cover"
                  />
                ) : (
                  <div className="w-44 h-44 hidden lg:flex text-white font-semibold bg-white/10 rounded-xl items-center justify-center">
                    NO IMAGE
                  </div>
                )}
              </div>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
}
