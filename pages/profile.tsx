import Layout from "@/components/Layout";
import { GetServerSideProps } from "next";
import { getServerSession } from "next-auth";
import nextAuthOptions from "@/lib/config/nextauth.config";
import Image from "next/image";
import { useState } from "react";
import { User } from "@/lib/types";
import axios from "axios";
import { useSession } from "next-auth/react";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getServerSession(ctx.req, ctx.res, nextAuthOptions);
  if (session === null || session.user === undefined) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }
  return { props: {} };
};

export default function Profile() {
  const session = useSession();
  const user = session.data?.user as User;
  return (
    <Layout>
      {user && (
        <div className="lg:px-20 lg:mt-4 font-ox mb-6">
          <div className="relative flex flex-col w-full bg-[#BABDCB26] rounded-[40px]">
            <div
              className="absolute inset-0 z-0 border-[2px] border-white rounded-[40px]"
              style={{
                WebkitMaskImage:
                  "linear-gradient(90deg, transparent 0%, rgba(255,255,255,0.2) 100%)",
              }}
            />
            <div className="h-14 lg:h-16 relative rounded-t-[40px] overflow-hidden flex items-center justify-center lg:bg-white/10">
              <div
                className="absolute top-0 left-0 h-full rouned-t-[40px] w-full border-[2px] border-white"
                style={{
                  WebkitMaskImage:
                    "linear-gradient(90deg, transparent 0%, rgba(255,255,255,0.2) 100%)",
                }}
              />
              <p className="font-ox text-white font-bold">PROFILE</p>
            </div>
            {/* flex */}
            <div className="flex-col lg:flex-row flex h-full px-6">
              <div className="w-full lg:w-1/2 lg:pr-6 space-y-6 lg:border-r-[3px] lg:border-white/20 pt-6">
                <div className="relative px-6 py-4 h-fit rounded-xl bg-white bg-opacity-5">
                  <div
                    className="absolute h-full top-0 left-0 w-full z-0 border-[2px] border-white rounded-lg"
                    style={{
                      WebkitMaskImage:
                        "linear-gradient(90deg, rgba(255,255,255,0.3) 0%, rgba(255,255,255,0.1) 100%)",
                    }}
                  />
                  <div className="flex space-x-6 w-full relative">
                    <div className="relative w-16 h-16 min-w-[4rem] rounded-xl bg-gradient-to-tr from-[#0057ff] to-[#00a3ff] border-2 border-white">
                      <Image
                        src="/logo.png"
                        alt="digital-footwear"
                        layout="fill"
                        className="object-cover"
                        style={{ WebkitFilter: "invert(100%)" }}
                      />
                    </div>
                    <div className="flex-grow truncate overflow-clip">
                      <p className="font-ox font-semibold text-white lg:text-lg">
                        {user.username}
                      </p>
                      <p className="text-white/50 font-ox truncate line-clamp-1">
                        {user.email}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="relative px-6 py-4 h-fit rounded-xl bg-white bg-opacity-5">
                  <div
                    className="absolute h-full top-0 left-0 w-full z-0 border-[2px] border-white rounded-lg"
                    style={{
                      WebkitMaskImage:
                        "linear-gradient(90deg, rgba(255,255,255,0.3) 0%, rgba(255,255,255,0.1) 100%)",
                    }}
                  />
                  <div className="space-y-2">
                    <p className="font-ox font-semibold text-white lg:text-lg">
                      Description
                    </p>
                    <p className="text-white/50 font-ox mt-2">User Account</p>

                    <p className="font-ox font-semibold text-white lg:text-lg pt-2">
                      Wallet
                    </p>
                  </div>
                </div>
                <div />
              </div>
            </div>
          </div>
        </div>
      )}
    </Layout>
  );
}
