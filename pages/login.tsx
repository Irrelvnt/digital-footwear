import Particles from "@/components/Particles";
import Button from "@/components/primitives/Button";
import Input from "@/components/primitives/Input";
import nextAuthOptions from "@/lib/config/nextauth.config";
import { Credentials } from "@/lib/types";
import { Transition } from "@headlessui/react";
import { GetServerSideProps } from "next";
import { getServerSession } from "next-auth";
import { signIn } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { Fragment, useState } from "react";
import { AiOutlineClose } from "react-icons/ai";
import { BiSolidError } from "react-icons/bi";
import { FaQuestion } from "react-icons/fa";
import { PiGear } from "react-icons/pi";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getServerSession(ctx.req, ctx.res, nextAuthOptions);
  if (session?.user !== undefined) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

export default function Login() {
  const router = useRouter();
  const [formValues, SetFormValues] = useState<Credentials>({
    username: "",
    password: "",
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    SetFormValues({ ...formValues, [e.target.name]: e.target.value });
  };
  return (
    <div className="relative pt-36 lg:pt-[17vh] w-full h-screen bg-bg ">
      <div className="absolute inset-0 z-0">
        <Particles />
      </div>
      <div
        aria-live="assertive"
        className="fixed left-0 right-0 bottom-0 top-4 md:top-10 z-50 flex px-4 py-6 pointer-events-none sm:p-6 sm:items-start"
      >
        <div className="w-full flex flex-col items-center space-y-4 sm:items-end">
          <Transition
            show={router.query.error !== undefined}
            as={Fragment}
            enter="transform ease-out duration-300 transition"
            enterFrom="translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
            enterTo="translate-y-0 opacity-100 sm:translate-x-0"
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="max-w-sm w-full bg-slate-700 shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden">
              <div className="p-4">
                <div className="flex items-start">
                  <div className="flex-shrink-0">
                    <BiSolidError
                      className="h-6 w-6 fill-red-500"
                      aria-hidden="true"
                    />
                  </div>
                  <div className="ml-3 w-0 flex-1 pt-0.5">
                    <p className="text-sm font-medium text-red-500">Error</p>
                    <p className="mt-1 text-sm text-gray-100">
                      {router.query.error}
                    </p>
                  </div>
                  <div className="ml-4 flex-shrink-0 flex">
                    <button
                      onClick={() => {
                        router.push(router.pathname);
                      }}
                      className="bg-slate-700 rounded-md inline-flex text-white hover:text-gray-200 focus:outline-none"
                    >
                      <span className="sr-only">Close</span>
                      <AiOutlineClose className="h-5 w-5" aria-hidden="true" />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Transition>
        </div>
      </div>
      <div className="absolute z-10 w-full left-0 top-0 flex items-center pt-10 justify-center lg:justify-between py-2 md:py-4 md:px-12">
        <div className="w-44 h-20 lg:w-20 flex items-center justify-center">
          <Image
            src="/logo.png"
            style={{ WebkitFilter: "invert(100%)" }}
            width={120}
            height={120}
            alt="digital-footwear"
          />
        </div>
        <div className="hidden lg:block w-12 h-12 p-2 hover:scale-105 cursor-pointer">
          <FaQuestion className="w-8 h-8 fill-white" />
        </div>
      </div>
      <div
        role="form"
        className="relative w-[90vw] lg:w-[35vw] overflow-clip rounded-2xl backdrop-blur-sm bg-white/10 px-4 pt-2 pb-4 lg:px-12 lg:py-6 mx-auto"
      >
        <div
          className="absolute z-0 inset-0 border-[3px] border-white/30 rounded-2xl"
          style={{
            WebkitMaskImage:
              "linear-gradient(90deg, transparent 0%, white 100%)",
          }}
        />
        <p className="text-white font-ox font-bold text-xl mb-2 mt-4 lg:mt-6">
          LOGIN
        </p>
        <div className="space-y-6 pt-6">
          <Input
            name="username"
            type="text"
            onChange={handleChange}
            placeholder="Username"
          />
          <Input
            name="password"
            onChange={handleChange}
            type="password"
            placeholder="Password"
          />
        </div>

        <div className="relative mx-auto w-fit mt-6">
          <Button
            type="submit"
            text="Login"
            onClick={async () => {
              await signIn("credentials", {
                ...formValues,
              });
            }}
          />
        </div>
        <div className="flex items-center justify-between mt-6">
          <Link href="/forgot">
            <p className="border-b-2 border-transparent hover:border-white transition cursor-pointer relative text-center text-lg font-medium text-white font-ox">
              CAN{"'"}T SIGN IN?
            </p>
          </Link>
          <Link href="/register">
            <p className="border-b-2 border-transparent hover:border-white transition cursor-pointer relative text-center text-lg font-medium text-white font-ox">
              CREATE ACCOUNT
            </p>
          </Link>
        </div>
      </div>
      <div className="absolute z-10 w-full left-0 bottom-0 flex items-center justify-center pb-16 lg:pb-0 lg:justify-between py-2 md:py-4 md:px-12">
        <div className="hidden lg:flex w-20 h-20 items-center justify-center">
          <PiGear className="w-8 h-8 fill-white" />
        </div>
        <div className="text-white w-fit font-ox font-bold text-xl lg:text-lg">
          v0.03
        </div>
      </div>
    </div>
  );
}
