import Button from "@/components/primitives/Button";
import Input from "@/components/primitives/Input";
import Loader from "@/components/primitives/Loader";
import { User } from "@/lib/types";
import { Transition } from "@headlessui/react";
import axios from "axios";
import { StatusCodes } from "http-status-codes";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { ChangeEvent, FormEvent, Fragment, useState } from "react";
import { AiOutlineClose } from "react-icons/ai";
import { BiSolidError } from "react-icons/bi";

export default function Register() {
  const router = useRouter();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>("");

  const [formValues, setFormValues] = useState({
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    adress: "",
    isSeller: false,
  });

  const changeEventHandler = ({
    target: { name, value },
  }: ChangeEvent<HTMLInputElement>) => {
    setFormValues((prev) => {
      return { ...prev, [name]: value };
    });
  };

  const Register = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (formValues.password !== formValues.confirmPassword) {
      return setError("passwords do not match");
    }

    try {
      setLoading(true);
      const res = await axios.post<User>("/api/auth/sign-up", {
        ...formValues,
      });

      if (res.status === StatusCodes.CREATED) {
        router.replace("/login");
      }
    } catch (error) {
      if (error instanceof axios.AxiosError) {
        setError("An error has occured, please try again later");
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="relative w-full min-h-screen bg-bg">
      <div
        aria-live="assertive"
        className="fixed left-0 right-0 bottom-0 top-4 md:top-10 z-50 flex px-4 py-6 pointer-events-none sm:p-6 sm:items-start"
      >
        <div className="w-full flex flex-col items-center space-y-4 sm:items-end">
          <Transition
            show={error !== ""}
            as={Fragment}
            enter="transform ease-out duration-300 transition"
            enterFrom="translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
            enterTo="translate-y-0 opacity-100 sm:translate-x-0"
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="max-w-sm w-full bg-slate-700 shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden">
              <div className="p-4">
                <div className="flex items-start">
                  <div className="flex-shrink-0">
                    <BiSolidError
                      className="h-6 w-6 fill-red-500"
                      aria-hidden="true"
                    />
                  </div>
                  <div className="ml-3 w-0 flex-1 pt-0.5">
                    <p className="text-sm font-medium text-red-500">Error</p>
                    <p className="mt-1 text-sm text-gray-100">{error}</p>
                  </div>
                  <div className="ml-4 flex-shrink-0 flex">
                    <button
                      className="bg-slate-700 rounded-md inline-flex text-white hover:text-gray-200 focus:outline-none"
                      onClick={() => {
                        setError("");
                      }}
                    >
                      <span className="sr-only">Close</span>
                      <AiOutlineClose className="h-5 w-5" aria-hidden="true" />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Transition>
        </div>
      </div>
      {loading && (
        <div className="absolute left-0 top-0 w-full h-full lg:hidden flex items-center justify-center bg-black/50 z-40">
          <Loader />
        </div>
      )}
      <form onSubmit={Register} className="min-h-screen flex items-center ">
        <div className="relative z-10 w-[90vw] my-auto lg:w-[50rem] h-fit md:bg-white/10 mx-auto md:rounded-2xl md:backdrop-blur-sm px-6 lg:px-14 pb-4 lg:pb-10 overflow-clip">
          <div
            className="hidden md:block absolute z-0 rounded-2xl inset-0 border-[2px] border-white/30"
            style={{
              WebkitMaskImage:
                "linear-gradient(90deg, transparent 0%, white 100%)",
            }}
          />
          {loading && (
            <div className="hidden absolute left-0 top-0 w-full h-full lg:flex items-center justify-center bg-black/50 z-40">
              <Loader />
            </div>
          )}
          <div className="relative z-10 flex items-center w-fit mx-auto pt-10 pb-10">
            <div className="w-44 h-20 lg:w-20 flex items-center justify-center">
              <Image
                src="/logo.png"
                style={{ WebkitFilter: "invert(100%)" }}
                width={120}
                height={120}
                alt="digital-footwear"
              />
            </div>
            <p className="hidden md:block font-ox text-4xl text-white font-black">
              digital-footwear
            </p>
          </div>

          <div className="space-y-6">
            <Input
              onChange={changeEventHandler}
              name="username"
              type="text"
              placeholder="Username"
            />
            <Input
              onChange={changeEventHandler}
              name="email"
              type="email"
              placeholder="Email"
            />
            <Input
              onChange={changeEventHandler}
              name="password"
              type="password"
              placeholder="password"
            />
            <Input
              onChange={changeEventHandler}
              name="confirmPassword"
              type="password"
              placeholder="Confirm password"
            />
            <Input
              onChange={changeEventHandler}
              name="adress"
              type="string"
              placeholder="Adress"
            />
            {/*
              toggle switch
            */}
            <div className="relative rounded-lg w-full h-fit bg-white/10 px-2 py-2 text-white flex items-center justify-between gap-4">
              <div
                onClick={() => {
                  setFormValues((prev) => {
                    return { ...prev, isSeller: true };
                  });
                }}
                className={`${
                  formValues.isSeller
                    ? "bg-white/80 text-purple-500"
                    : "bg-transparent"
                } flex-1 text-center py-2 rounded-md cursor-pointer transition`}
              >
                Seller
              </div>
              <div
                onClick={() => {
                  setFormValues((prev) => {
                    return { ...prev, isSeller: false };
                  });
                }}
                className={`${
                  !formValues.isSeller
                    ? "bg-white/80 text-purple-500"
                    : "bg-transparent"
                } flex-1 text-center py-2 rounded-md cursor-pointer transition`}
              >
                Buyer
              </div>
            </div>

            <div className="w-fit mx-auto py-6">
              <Button text="NEXT" type="submit" />
            </div>
          </div>

          <div className="mx-auto relative flex w-fit text-white/90 space-x-3 font-ox font-semibold pb-2">
            <p>Already Have an Account?</p>
            <Link href="/login">
              <p className="border-b-2 cursor-pointer hover:border-white/90 border-transparent transition">
                Login
              </p>
            </Link>
          </div>
          <div className="text-center relative text-white/50 font-ox font-normal text-xs mt-4 lg:mt-0">
            By signing up, your agree to Partie{"’"}s{" "}
            <a className="text-white inline-block">
              Terms and Conditions, End-User License Agreement{" "}
            </a>{" "}
            and{" "}
            <Link href="/eula">
              <p className="text-white inline-block cursor-pointer">
                Privacy Policy.
              </p>
            </Link>
          </div>
        </div>
      </form>
    </div>
  );
}
