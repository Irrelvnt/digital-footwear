import Particles from "@/components/Particles";
import SEO from "@bradgarropy/next-seo";
import { useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import {
  AiFillFacebook,
  AiFillYoutube,
  AiOutlineInstagram,
} from "react-icons/ai";
import { FaDiscord } from "react-icons/fa";
import { RiTwitterXFill } from "react-icons/ri";

const Main = () => {
  const session = useSession();
  const [slide, setSlide] = useState<number>(0);
  useEffect(() => {
    const interval = setInterval(() => {
      setSlide((slide + 1) % 3);
    }, 3000);

    return () => {
      clearInterval(interval);
    };
  }, [slide]);

  return (
    <div className="relative h-screen w-full flex items-center bg-bg">
      <SEO
        title="digital-footwear"
        description="Welcome to digital-footwear"
        keywords={[
          "digital-footwear",
          "smart contracts",
          "ecommerce",
          "shop",
          "worldwide",
          "crypto",
          "shop in style",
          "pay in crypto",
          "save up",
        ]}
      />
      <div className="absolute z-10 inset-0 top-16">
        <Particles />
      </div>
      <div className="absolute font-semibold text-white font-ox text-xl italic top-0 h-12 bg-slate-50/20 w-screen z-20 flex items-center px-6 lg:px-16">
        <div className="w-10 h-10 mt-3 overflow-clip mr-2">
          <Image
            src="/logo.png"
            alt="logo"
            style={{ WebkitFilter: "invert(100%)" }}
            className="scale-125"
            width={120}
            height={120}
          />
        </div>
        <p>DIGITAL FOOTWEAR</p>
        <div className="flex-grow" />
        {session.status === "authenticated" ? (
          <Link href="/shop">
            <p className="font-normal text-white/80 text-base cursor-pointer hover:text-white transition">
              Continue
            </p>
          </Link>
        ) : (
          <Link href="/login">
            <p className="font-normal text-white/80 text-base cursor-pointer hover:text-white transition">
              Login
            </p>
          </Link>
        )}
      </div>
      <div className="w-[80vw] z-30 flex flex-col items-center justify-center h-80 md:h-96 md:min-w-[50%] relative mx-auto">
        <div className="relative min-w-[20rem] overflow-hidden w-full h-24">
          <div
            style={{
              transform: `translateY(-${6 * slide}rem)`,
            }}
            className="absolute w-full flex items-center flex-col h-24 transition duration-1000"
          >
            <div className="text-4xl italic lg:text-7xl text-center w-full font-semibold font-ox text-white min-h-[6rem] h-24">
              DIGITAL FOOTWEAR
            </div>
            <div className="text-4xl lg:text-7xl text-center w-full font-semibold font-ox text-white min-h-[6rem] h-24">
              Your one stop shop
            </div>
            <div className="text-4xl lg:text-7xl text-center w-full font-semibold font-ox text-white min-h-[6rem] h-24">
              Available worldwide
            </div>
          </div>
        </div>
        <div className="md:w-4/5 flex items-center justify-center mb-12 mt-5">
          <div className="text-xl lg:text-4xl font-normal font-ox text-white/70">
            Shop with confidence
          </div>
          <div className="ml-2 mt-2 lg:ml-0 space-x-2 lg:space-x-4 flex scale-50">
            <div
              onClick={() => {
                setSlide(0);
              }}
              className="rounded-full h-4 w-4 transition cursor-pointer"
              style={{
                backgroundColor: slide === 0 ? "white" : "#ffffff55",
              }}
            />
            <div
              onClick={() => {
                setSlide(1);
              }}
              className="rounded-full h-4 w-4 transition cursor-pointer"
              style={{
                backgroundColor: slide === 1 ? "white" : "#ffffff55",
              }}
            />
            <div
              onClick={() => {
                setSlide(2);
              }}
              className="rounded-full h-4 w-4 transition cursor-pointer"
              style={{
                backgroundColor: slide === 2 ? "white" : "#ffffff55",
              }}
            />
          </div>
        </div>
      </div>
      <div className="absolute bottom-12 w-full ">
        <div className="scale-90 md:scale-100 bg-gradient-to-tr from-slate-200/10 to-slate-100/10 backdrop-blur-sm rounded-[40px] px-6 py-2 w-64 md:w-[30rem] mx-auto z-40 relative">
          <div className="flex w-full items-center justify-between fill-white">
            <AiOutlineInstagram className="w-9 h-9 cursor-pointer transition fill-inherit rounded-full bg-transparent hover:bg-white/90 p-1 hover:fill-pink-500" />
            <AiFillFacebook className="w-9 h-9 cursor-pointer transition fill-inherit rounded-full bg-transparent hover:bg-white/90 p-1 hover:fill-[#0866ff]" />
            <FaDiscord className="w-9 h-9 cursor-pointer transition fill-inherit rounded-full bg-transparent hover:bg-white/90 p-1 hover:fill-[#5561f5]" />
            <RiTwitterXFill className="w-9 h-9 cursor-pointer transition fill-inherit rounded-full bg-transparent hover:bg-white/90 p-1 hover:fill-slate-700" />
            <AiFillYoutube className="w-9 h-9 cursor-pointer transition fill-inherit rounded-full bg-transparent hover:bg-white/90 p-1 hover:fill-red-500" />
          </div>
        </div>
      </div>
    </div>
  );
};
export default Main;
