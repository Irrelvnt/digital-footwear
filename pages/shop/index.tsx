import Layout from "@/components/Layout";
import nextAuthOptions from "@/lib/config/nextauth.config";
import { Product } from "@prisma/client";
import axios from "axios";
import { GetServerSideProps } from "next";
import { getServerSession } from "next-auth";
import { useEffect, useState } from "react";
import ShopFilter from "@/components/shop/ShopFilter";
import productItem from "@/components/shop/ProductItem";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getServerSession(ctx.req, ctx.res, nextAuthOptions);
  if (session === null || session.user === undefined) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

export default function Shop() {
  const [categories, setCategories] = useState<string[]>(["All Categories"]);
  const [loading, setLoading] = useState(true);
  const [filter, setFilter] = useState(categories[0]);
  const [data, setData] = useState<Product[]>([]);

  const products =
    filter === categories[0]
      ? data
      : data.filter(
          (product) => product.category.toLowerCase() === filter.toLowerCase()
        );

  useEffect(() => {
    if (data.length == 0) {
      setLoading(true);
      axios
        .get<{ products: Product[] }>("/api/products")
        .then(({ data }) => {
          setData(data.products);
        })
        .catch((error) => {})
        .finally(() => setLoading(false));
    }

    if (categories.length == 1) {
      axios
        .get("/api/products/categories")
        .then(({ data }) => {
          setCategories((prev) => [...prev, ...data.categories]);
        })
        .catch((error) => {});
    }
  }, [categories.length, data.length]);

  const applyFilter = (e: any) => {
    if (e.target.value == filter) {
      return;
    }
    setFilter(e.target.value);
  };

  return (
    <Layout>
      <div className="w-full lg:pl-16 lg:pr-12 xl:pl-24 xl:pr-16">
        <div className="w-full items-center py-3 lg:py-6 md:flex ">
          <div className="flex relative z-40">
            {ShopFilter(categories, filter, applyFilter)}
          </div>
        </div>
        {loading && (
          <div className="w-full grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 xl:gap-10 lg:overflow-y-auto pb-6">
            {new Array(12).fill(0).map((_, i) => (
              <div
                key={i}
                className="w-full  h-56 lg:h-64 bg-white/20 rounded-xl animate-pulse"
              />
            ))}
          </div>
        )}
        {!loading && (
          <div className="w-full h-full grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 xl:gap-6 lg:overflow-y-auto pb-6">
            {products.map(productItem)}
          </div>
        )}
      </div>
    </Layout>
  );
}
