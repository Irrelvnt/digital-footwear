import Layout from "@/components/Layout";
import Input from "@/components/primitives/Input";
import Stars from "@/components/stars";
import useCheckout from "@/hooks/useCheckout";
import { Product } from "@prisma/client";
import axios from "axios";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { BiLock } from "react-icons/bi";
import nextAuthOptions from "@/lib/config/nextauth.config";
import { GetServerSideProps } from "next";
import { getServerSession } from "next-auth";
import { getRating } from "@/utils/getRating";
import { useSession } from "next-auth/react";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getServerSession(ctx.req, ctx.res, nextAuthOptions);
  if (session === null || session.user === undefined) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

export default function Checkout() {
  const router = useRouter();
  const session = useSession();
  const { handleChange, handleSubmit } = useCheckout();
  const [sellerAdress, setSellerAdress] = useState<string | null>(null);
  const [product, setProduct] = useState<Product | null>(null);
  const [rating, setRating] = useState({ value: 0, count: 0 });
  const [myAdress, setMyAdress] = useState<string | null>(null);
  useEffect(() => {
    const getAdress = async () => {
      const adress = await axios
        .get("/api/users/" + session.data?.user?.id)
        .then(({ data }) => data.user.adress)
        .catch(() => null);
      if (adress === null) {
        return;
      }
      setMyAdress(adress);
    };
    getAdress();
  }, [session.data?.user?.id]);
  if (product == null) {
    axios
      .get<{ product: Product }>(`/api/products/${router.query.id}`)
      .then(({ data }) => setProduct(data.product))
      .catch(function () {});
  }

  useEffect(() => {
    const getSellerRatingInfo = async (sellerId: string) => {
      const sellerAdress = await axios
        .get("/api/users/" + sellerId)
        .then(({ data }) => {
          const adress = data.user.adress;
          return adress;
        })
        .catch(() => null);
      if (sellerAdress === null) {
        return;
      }
      setSellerAdress(sellerAdress);
      const sellerRatingInfo = await getRating(sellerAdress);
      if (!sellerRatingInfo) return;
      // @ts-ignore
      setRating(sellerRatingInfo);
    };

    if (product !== null) {
      getSellerRatingInfo(product.sellerId);
    }
  }, [product]);

  return (
    <Layout>
      <div className="lg:px-20 lg:mt-4 font-ox">
        <div className="relative flex flex-col w-full sm:bg-[#BABDCB26] rounded-[40px]">
          <div
            className="absolute inset-0 z-0 sm:border-[2px] sm:border-white rounded-[40px]"
            style={{
              WebkitMaskImage:
                "linear-gradient(90deg, transparent 0%, rgba(255,255,255,0.2) 100%)",
            }}
          />
          <div className="h-14 lg:h-16 relative rounded-t-[40px] overflow-hidden flex items-center justify-center lg:bg-white/20">
            <div
              className="absolute top-0 left-0 h-full rouned-t-[40px] w-full sm:border-[2px] sm:border-white"
              style={{
                WebkitMaskImage:
                  "linear-gradient(90deg, transparent 0%, rgba(255,255,255,0.2) 100%)",
              }}
            />
            <p className="font-ox text-white text-semibold">CHECKOUT</p>
          </div>
          {product == null ? (
            <div className="min-w-full min-h-full ">
              <div className="flex-col lg:flex-row flex h-full px-2 sm:px-6">
                <div className=" w-full lg:w-1/2 flex space-x-4 lg:pr-6 lg:space-x-0 lg:space-y-6 lg:flex-col lg:border-r-[3px] border-white/20 pt-6">
                  <div className="animate-pulse relative w-1/2 min-w-[50%] lg:w-full bg-gradient-to-tr from-white/20 to-white/10 h-32 lg:h-[20rem] rounded-xl overflow-clip">
                    <Image
                      alt="card"
                      src={/* product.image */ "/logo.png"}
                      layout="fill"
                      className="object-contain relative z-20"
                    />
                    <div className="absolute inset-0 z-10 bg-black/30 rounded-xl" />
                    <Image
                      alt="card"
                      src={/* product.image */ "/logo.png"}
                      layout="fill"
                      className="object-cover blur-sm"
                    />
                  </div>
                  <div className="h-full w-1/2 lg:w-full flex flex-col gap-1.5">
                    <div className="font-ox font-semibold text-white lg:text-lg">
                      <div className="h-[3ch] w-[10ch] bg-gray-400 animate-pulse rounded-md" />
                    </div>
                    <div className="font-ox font-normal text-white lg:text-lg">
                      <div className="h-[3ch] w-[22ch] bg-gray-400 animate-pulse rounded-md" />
                    </div>
                    <div className="h-[2ch] w-[10ch] bg-gray-400 animate-pulse rounded-md" />
                  </div>
                </div>
                <div className="w-full lg:w-1/2 h-full lg:pl-6 pt-6 space-y-3">
                  <div className="bg-gray-400 animate-pulse py-6 px-3 rounded-md" />
                  <div className="bg-gray-400 animate-pulse py-6 px-3 rounded-md" />

                  <div className="hidden lg:block lg:pt-2">
                    <div className="py-5 full bg-green-300 animate-pulse rounded-md" />
                    <div className="flex items-center space-x-4 w-fit mx-auto lg:mb-8">
                      <BiLock className="fill-white/70 w-3 h-3" />
                      <p className="font-ox text-xs text-white/70 font-thin">
                        Payment in tokens only
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="lg:hidden px-2 sm:px-6 mt-3">
                <button
                  type="submit"
                  className="relative mt-3 mb-4 bg-[#6adf92] w-full group shadow-sm focus:outline-none focus:ring-none text-white py-[0.6rem] px-[2.5rem] rounded-lg"
                >
                  <div className="relative z-10 transition font-bold flex items-center w-fit mx-auto">
                    Pay{" "}
                    <div className="text-transparent animate-pulse">...</div>
                    <div className="h-[5ch] w-[12ch]" />
                  </div>
                </button>
                <div className="flex items-center space-x-4 w-fit mx-auto mb-4">
                  <BiLock className="fill-white/70 w-3 h-3" />
                  <p className="font-ox text-xs text-white/70 font-thin">
                    Payment in tokens only
                  </p>
                </div>
              </div>
            </div>
          ) : (
            <form onSubmit={handleSubmit} className="w-full h-full">
              <div className="flex-col lg:flex-row flex h-full px-2 sm:px-6">
                <div className=" w-full lg:w-1/2 flex space-x-4 lg:pr-6 lg:space-x-0 lg:space-y-6 lg:flex-col lg:border-r-[3px] border-white/20 pt-6">
                  <div className="relative w-1/2 min-w-[50%] lg:w-full bg-gradient-to-tr from-white/70 to-white/40 h-32 lg:h-[20rem] rounded-xl overflow-clip">
                    <Image
                      alt="card"
                      src={product.image}
                      layout="fill"
                      className="object-contain relative z-20"
                    />
                  </div>
                  <div className="h-full w-1/2 lg:w-full flex flex-col">
                    <p className="font-ox font-semibold text-white lg:text-lg">
                      {product.name}
                    </p>
                    <p className="font-ox font-normal text-white lg:text-lg">
                      {product.description}
                    </p>
                    <p className="font-normal text-sm text-white/60">
                      {rating.count} ratings
                    </p>
                    <Stars
                      rating={rating.value}
                      sellerAdress={sellerAdress}
                      buyerAdress={myAdress}
                    />
                  </div>
                </div>
                <div className="w-full lg:w-1/2 h-full lg:pl-6 pt-6 space-y-3">
                  <Input
                    placeholder="Full name"
                    onChange={handleChange}
                    name="name"
                  />
                  <Input
                    placeholder="Email adress"
                    type="email"
                    onChange={handleChange}
                    name="email"
                  />

                  <div className="hidden lg:block lg:pt-2">
                    <button
                      type="submit"
                      className="relative mt-2 mb-4 bg-[#6adf92] w-full group shadow-sm focus:outline-none focus:ring-none text-white py-[0.6rem] px-[2.5rem] rounded-lg"
                    >
                      <div className="relative z-10 flex items-center w-fit mx-auto transition font-bold gap-2">
                        Pay {product.price.amount}
                        <div className="relative w-7 h-7 -mx-1 flex items-center">
                          {product.price.currency}
                        </div>
                      </div>
                    </button>
                    <div className="flex items-center space-x-4 w-fit mx-auto lg:mb-8">
                      <BiLock className="fill-white/70 w-3 h-3" />
                      <p className="font-ox text-xs text-white/70 font-thin">
                        Payment in ETH only
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="lg:hidden px-2 sm:px-6 mt-3">
                <button
                  type="submit"
                  className="relative mt-3 mb-4 bg-[#6adf92] w-full group shadow-sm focus:outline-none focus:ring-none text-white py-[0.6rem] px-[2.5rem] rounded-lg"
                >
                  <div className="relative z-10 transition font-bold flex items-center w-fit mx-auto">
                    Pay {product.price.amount}
                    {product.price.currency}
                  </div>
                </button>
                <div className="flex items-center space-x-4 w-fit mx-auto mb-4">
                  <BiLock className="fill-white/70 w-3 h-3" />
                  <p className="font-ox text-xs text-white/70 font-thin">
                    Payment in ETH only
                  </p>
                </div>
              </div>
            </form>
          )}
        </div>
      </div>
    </Layout>
  );
}
