import { NextApiHandler } from "next";
import NextAuth from "next-auth/next";
import nextAuthOptions from "@/lib/config/nextauth.config";

const authHandler: NextApiHandler = (req, res) =>
  NextAuth(req, res, nextAuthOptions);

export default authHandler;
