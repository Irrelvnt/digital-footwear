import { Login } from "@/handlers/auth.handlers";
import { createRouter } from "next-connect";
import { HttpStatusCode } from "axios";
import { ValidationMiddleWare } from "@/middlewares/validation.middleware";
import { CredentialsObjectValidator } from "@/lib/types";

import type { NextApiRequest, NextApiResponse } from "next";

const router = createRouter<NextApiRequest, NextApiResponse>();

router
  .use((Request, Response, next) =>
    ValidationMiddleWare(Request, Response, next, CredentialsObjectValidator)
  )
  .post(Login);

export default router.handler({
  onError: (err: any, req: NextApiRequest, res: NextApiResponse) => {
    console.error(err.stack);
    res
      .status(err.statusCode || 500)
      .end(`something broke! message:  ${err.message}`);
  },
  onNoMatch: (req: NextApiRequest, res: NextApiResponse) => {
    res.status(HttpStatusCode.NotFound).end(`Route not found`);
  },
});
