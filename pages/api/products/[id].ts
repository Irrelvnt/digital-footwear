import { createRouter } from "next-connect";
import { HttpStatusCode } from "axios";

import type { NextApiRequest, NextApiResponse } from "next";
import { GetProduct } from "@/handlers/product.handlers";
import { AuthGuard } from "@/middlewares/auth.middleware";

const router = createRouter<NextApiRequest, NextApiResponse>();

router.use(AuthGuard).get(GetProduct);

export default router.handler({
  onError: (err: any, req: NextApiRequest, res: NextApiResponse) => {
    console.error(err.stack);
    res
      .status(err.statusCode || 500)
      .end(`something broke! message:  ${err.message}`);
  },
  onNoMatch: (req: NextApiRequest, res: NextApiResponse) => {
    res.status(HttpStatusCode.NotFound).end(`Route not found`);
  },
});
